/* ejercicio: Dado un numero ingresado por el usuario
    1. Imprimir los numeros desde -a hasta a
    2. Imprimir si es por o impar
    3. Imprimir el numero que sigue y el anterios.
*/

let a,i;
a = prompt("Valor:");
a = parseInt(a);
i=-a;
while(i<=a){
    document.write(i+"<br>");
    i++;
}
// para números negativos 
while(i>=a){
    document.write(i+"<br>");
    i = i-1;
}

if(a%2 === 0){
    document.write("PAR<br>");
}else{
    document.write("IMPAR<br>");
}
document.write(a-1);
document.write(a+1);